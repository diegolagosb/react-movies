import styles from "./App.module.css";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { MovieDetails } from "../pages/MovieDetails";
import { LandingPage } from "../pages/LandingPage";
import React from "react";

export function App() {
  return (
    <React.StrictMode>
      <Router>
        <header className={styles.header}>
          <Link to="/" className={styles.link}>
            <h1 className={styles.title}>Nefli</h1>
          </Link>
        </header>
        <main>
          <Switch>
            <Route exact path="/movies/:movieId">
              <MovieDetails />
            </Route>
            <Route path="/">
              <LandingPage />
            </Route>
          </Switch>
        </main>
      </Router>
    </React.StrictMode>
  );
}
