import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Spinner } from "../components/Spinner";
import { get } from "../utils/httpClient";
import styles from "./MovieDetails.module.css";
import movieTrailer from "movie-trailer";

export function MovieDetails() {
  const { movieId } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [movie, setMovie] = useState(null);
  const [trailer, setTrailer] = useState(null);

  useEffect(() => {
    setIsLoading(true);

    get("/movie/" + movieId).then((data) => {
      setMovie(data);
      setIsLoading(false);
    });
  }, [movieId]);

  if (isLoading) {
    return <Spinner />;
  }

  const getYoutubeTrailerLink = async () => {
    const youtubeLink = await movieTrailer(movie.title);
    setTrailer(youtubeLink);
  };

  getYoutubeTrailerLink();

  const formattedTrailerLink = (link) => {
    return link?.replace("watch?v=", "embed/");
  };

  const imageUrl = "http://image.tmdb.org/t/p/w500" + movie.poster_path;

  let iframeTrailer = trailer ? (
    <>
      <h2 className={styles.trailerTitle}>Trailer</h2>
      <div className={styles.trailerSection}>
        <div className={styles.trailerContainer}>
          <iframe
            className={styles.responsiveIframe}
            width="1280"
            height="720"
            src={formattedTrailerLink(trailer)}
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen="true"
          ></iframe>
        </div>
      </div>
    </>
  ) : (
    <></>
  );

  return (
    <>
      <div className={styles.detailsContainer}>
        <img className={styles.imgCol} src={imageUrl} alt={movie.title} />
        <div className={styles.textCol}>
          <p className={styles.firsItem}>
            <strong>Title:</strong> {movie.title}
          </p>
          <strong>Genres:</strong>{" "}
          {movie.genres.map((genre) => genre.name).join(", ")}
          <p className={styles.overview}>
            <strong>Description:</strong> {movie.overview}
          </p>
          <p>
            <strong>Release date:</strong>{" "}
            {movie.release_date.split("-").reverse().join("-")}
          </p>
          <p>
            <strong>Runtime:</strong> {movie.runtime} minutes
          </p>
        </div>
      </div>
      {iframeTrailer}
    </>
  );
}
